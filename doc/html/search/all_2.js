var searchData=
[
  ['check_5fhist_5fcompatibility',['check_hist_compatibility',['../root__utils_8h.html#aa4dfacd85fe3761c4bd22ac46598110c',1,'check_hist_compatibility(TH1F *hData, TH1F *hMC, double textXPos=0.12, bool displayChi2=true, float xMin=0, float xMax=0, int nFuncPar=0, TH2D *hInvCov=nullptr):&#160;root_utils.cxx'],['../root__utils_8h.html#ad8691632d7734900ddf39a9b49be3a84',1,'check_hist_compatibility(TH1F *, TF1 *, double textXPos=0.12, bool displayChi2=true, float xMin=-1, float xMax=-1):&#160;root_utils.cxx']]],
  ['check_5fhist_5ffor_5foutliers',['check_hist_for_outliers',['../root__utils_8h.html#a2ff453befd14335e32c3f78981a2493a',1,'root_utils.cxx']]],
  ['check_5finput_5fparameters',['check_input_parameters',['../classhex__plotter.html#a92ea8b20434d1e90fbdf25ba12927b4e',1,'hex_plotter']]],
  ['clear_5fdata',['clear_data',['../classhex__values.html#a5f7e7f3a3114774e4cfa6b1e8632e58c',1,'hex_values']]],
  ['compare_5fstrings',['compare_strings',['../cpp__utils_8h.html#aa15346a1f01cd234f6102240a0cea051',1,'cpp_utils.cxx']]],
  ['contains',['contains',['../cpp__utils_8h.html#aa6871cf3b037e03028a847452afe76e2',1,'cpp_utils.cxx']]],
  ['convert_5fhist_5fto_5fmatrix',['convert_hist_to_matrix',['../root__utils_8h.html#a5acec75dcfd64485bf2c1eec4db1a686',1,'root_utils.cxx']]],
  ['convert_5fmatrix_5fto_5fhist',['convert_matrix_to_hist',['../root__utils_8h.html#acca3f8e28670de819ba52c027eac6bab',1,'root_utils.cxx']]],
  ['cpp_5futils_2eh',['cpp_utils.h',['../cpp__utils_8h.html',1,'']]],
  ['crop_5fpolygon',['crop_polygon',['../root__utils_8h.html#a5d416ef2ca80aec3c88b06a7afbbaf07',1,'root_utils.cxx']]],
  ['crop_5fpolygon_5fabove',['crop_polygon_above',['../root__utils_8h.html#a66ae86db10308cecc8ad140b97237738',1,'root_utils.cxx']]],
  ['cumulative_5fbin_5fresidual',['cumulative_bin_residual',['../root__utils_8h.html#a496b1a11ace2c0b69da62bb62b9c7400',1,'root_utils.cxx']]],
  ['cut_5foff_5fbins',['cut_off_bins',['../root__utils_8h.html#a5f1b31d44d1567b3fd505f1b85c5078d',1,'root_utils.cxx']]]
];
