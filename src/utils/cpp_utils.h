/**
        @file cpp_utils.h
        @brief A collection of C++ helper functions
  @author Andreas Alexander Maier
*/

#ifndef CPP_UTILS_H
#define CPP_UTILS_H

#include <algorithm>
#include <cctype>
#include <cmath>
#include <cstdarg>
#include <cstring>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <limits>
#include <regex>
#include <sstream>
#include <string>
#include <vector>

#ifdef _WIN32
const bool IS_WINDOWS_COMPILATION = true;
#include <stdint.h>
#include <windows.h>
const int MAX_INTEGER = INT_MAX;
const double MAX_DOUBLE = DBL_MAX;
#else
#include <unistd.h>
const bool IS_WINDOWS_COMPILATION = false;
const int MAX_INTEGER = std::numeric_limits<int>::max();
const double MAX_DOUBLE = std::numeric_limits<double>::max();
#endif

const uint64_t MAX_HASH_SIZE = uint64_t(exp2(32));  // well below max uint64_t
const uint64_t HASH_PRIME = uint64_t(exp2(16) + 1);

#define ANSI_COLOR_RESET "\x1b[0m"
#define ANSI_COLOR_BLACK "\x1b[30m"
#define ANSI_COLOR_RED "\x1b[31m"
#define ANSI_COLOR_GREEN "\x1b[32m"
#define ANSI_COLOR_YELLOW "\x1b[33m"
#define ANSI_COLOR_BLUE "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN "\x1b[36m"
#define ANSI_COLOR_WHITE "\x1b[37m"
#define ANSI_COLOR_BOLDBLACK "\033[1m\033[30m"
#define ANSI_COLOR_BOLDRED "\033[1m\033[31m"
#define ANSI_COLOR_BOLDGREEN "\033[1m\033[32m"
#define ANSI_COLOR_BOLDYELLOW "\033[1m\033[33m"
#define ANSI_COLOR_BOLDBLUE "\033[1m\033[34m"
#define ANSI_COLOR_BOLDMAGENTA "\033[1m\033[35m"
#define ANSI_COLOR_BOLDCYAN "\033[1m\033[36m"
#define ANSI_COLOR_BOLDWHITE "\033[1m\033[37m"

#define LOCATION (get_file_name(__FILE__, 1) + ": " + int_to_string(__LINE__) + " in " + __FUNCTION__).c_str()
#define DEBUG_MSG(x, ...)                                                                                              \
  {                                                                                                                    \
    if (verbose > 1) {                                                                                                 \
      switch_color("MAGENTA");                                                                                         \
      fprintf(stderr, "[DEBUG]");                                                                                      \
      fprintf(stderr, " (%s)", LOCATION);                                                                              \
      switch_color("RESET");                                                                                           \
      fprintf(stderr, " " x "\n", ##__VA_ARGS__);                                                                      \
    }                                                                                                                  \
  }
#define INFO_MSG(x, ...)                                                                                               \
  {                                                                                                                    \
    if (verbose > 0) {                                                                                                 \
      switch_color("BLUE");                                                                                            \
      fprintf(stderr, "[INFO]");                                                                                       \
      fprintf(stderr, " (%s)", LOCATION);                                                                              \
      switch_color("RESET");                                                                                           \
      fprintf(stderr, " " x "\n", ##__VA_ARGS__);                                                                      \
    }                                                                                                                  \
  }
#define STANDARD_MSG(x, ...)                                                                                           \
  {                                                                                                                    \
    fprintf(stderr, "[MESSAGE]");                                                                                      \
    fprintf(stderr, " " x "\n", ##__VA_ARGS__);                                                                        \
  }
#define WARNING_MSG(x, ...)                                                                                            \
  {                                                                                                                    \
    switch_color("YELLOW");                                                                                            \
    fprintf(stderr, "[WARNING]");                                                                                      \
    fprintf(stderr, " (%s)", LOCATION);                                                                                \
    switch_color("RESET");                                                                                             \
    fprintf(stderr, " " x "\n", ##__VA_ARGS__);                                                                        \
  }
#define ERROR_MSG(x, ...)                                                                                              \
  {                                                                                                                    \
    switch_color("RED");                                                                                               \
    fprintf(stderr, "[ERROR]");                                                                                        \
    fprintf(stderr, " (%s)", LOCATION);                                                                                \
    switch_color("RESET");                                                                                             \
    fprintf(stderr, " " x "\n", ##__VA_ARGS__);                                                                        \
  }

/// Prints a progress bar
void print_progress_bar(int now, int max);
/// Extracts all digits in string into an arrat
void string_to_array(const std::string& str, int arr[]);
/// Extracts all floating point numbers in string into an arrat
void string_to_array_double(const std::string& str, double arr[]);
/// Updates a value in a two column data file
void replace_val_two_col_file(const std::string& fileName, const std::string& searchKey, const std::string& newVal);
/// Switches console text color for windows and linux
void switch_color(const std::string& color);
/// Show example available colors
void print_colors();
/// Print to stdout and to file
void printf_file(const std::string& text, std::ofstream& fileout);
/// Checks if a string starts with a string
bool starts_with(const std::string& a, const std::string& b);
/// Checks if a string ends with a string
bool ends_with(std::string a, std::string b);
/// Checks if a string contains a string
bool contains(const std::string& a, const std::string& b);
/// Check if the string matches the given regex
bool match(const std::string& s, const std::string& r);
/// Checks if a file is empty
bool file_is_empty(const std::string& fileName);
/// Checks if a file exists
bool is_file(const std::string& fileName);
/// Checks if a file exists
bool is_file(char* fileName);
/// Checks if two strings are equal
bool compare_strings(const std::string& a, const std::string& b);
/// Checks if element is in array
bool is_in_array(int element, int arr[], int arraySize);
/// Checks if  elements given after arraySize are in array, regardless of order
bool is_same_elements(const int arr[2], int badOne, int badTwo);
/// Checks if element is in vector
bool is_in_vector(int num, std::vector<int> vec);
/// Checks if element is in vector
bool is_in_vector(double, std::vector<double>);
/// Command line user yes/no input request
bool yes_no_request(const std::string& prompt = "Do you want to continue?");
/// Get the mean of a vector
double get_mean(const std::vector<double>& v);
/// Get the error of the mean of a vector
double get_mean_err(const std::vector<double>& v);
/// Get the median of a vector
double get_median(std::vector<double>& v);
/// Get the error of the median of a vector
double get_median_err(std::vector<double>& v);

/// Counts number of digits in a string
int get_n_digits_in_string(std::string str);
/// Returns nth digit found in string
int digits_from_string(std::string str, int nth = 0);
/// Returns sign of a value
int sign(double v);
/// Returns the number of data columns in a file
int get_n_columns(const std::string& fileName, const std::string& delim = "\t");
/// Returns the number of appearances of a string in a string
int get_n_instance(const std::string& a, const std::string& b, bool omitRepetitions = false);
/// Returns the positive modulo
int positive_modulo(int i, int n);
/// Get argc as if parsed from command line
int get_argc(const std::string& cmd);

/// Returns the module of the multiplication of two big numbers (a * b) % c
uint64_t big_mod(uint64_t a, uint64_t b, uint64_t c);
/// A custom hash function to ensure distribution independent equal hashes for equal input
uint64_t modulo_hash(uint64_t i);
/// Merge two hash values
uint64_t modulo_merge_hash(uint64_t i, uint64_t j);
/// A custom hash function reinterpreting modified double as integer
uint64_t modulo_hash_double(double d);

/// Returns current directory
std::string get_current_dir();
/// Convert int to string
std::string int_to_string(int number, unsigned int digits = 0);
/// Convert float to string
std::string float_to_string(float number, unsigned int nDigitsAfterPoint = 1);
/// Convert double to string
std::string double_to_string(double number, unsigned int nDigitsAfterPoint = 1);
/// Properly print value with name, unit and sign. digits
std::string print_value(const std::string& valueName, double value, double unc, std::string unitName = "",
                        int nSigDigits = -1);
/// Trim whitespace from the start of string
std::string& ltrim(std::string& s);
/// Trim whitespace from the end of string
std::string& rtrim(std::string& s);
/// Trim whitespace from both ends of string
std::string& trim(std::string& s);
/// Get basename of a file
std::string get_file_name(const std::string& s, const int nLayers = 0);
/// Get basename of a file without extension
std::string get_file_name_no_ext(const std::string& s, const std::string& delim = ".", const int nLayers = 0);
/// Get dirname of a file
std::string get_file_dir(const std::string& s);
/// Get string before last instance of delimiter
std::string strip_extension(const std::string& s, const std::string& delim = ".");
/// Get string after last instance of delimiter
std::string get_extension(const std::string& s, const std::string& delim = ".", const int nLayers = 0);
/// Remove first instance of removeString from string
std::string remove_string(const std::string& s, const std::string& removeString = " ");
/// Remove all instances of removeChar from string
std::string remove_string_all(const std::string& s, const std::string& removeChar = "'");
/// Replace the first instance of substring in string
std::string replace_string(const std::string& s, const std::string& from = "", const std::string& to = "");
/// Replace all instances of substring in string
std::string replace_string_all(const std::string& s, const std::string& from = "", const std::string& to = "");
/// Converts floating point hours to hours:minutes
std::string hours_minutes(double decimalHours);
/// Returns correct enumeration suffix, e.g. 'st' for 1, 'nd' for 2
std::string enum_suffix(int times);
/// Get argv as if parsed from command line
std::vector<std::string> get_argv(const std::string& cmd);
/// Splits a string into pieces with a delimiter.
/// If delimiter is space, expressions in quotes ("") are considered one unit.
std::vector<std::string> split(const std::string& s, char delim);

/// Returns absolute value of a value
double positive(double v);
/// Extract first double from string
double double_from_string(const std::string& s);

#endif
